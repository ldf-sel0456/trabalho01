# Trabalho 01

O objetivo do trabalho era criar 3 funções na linguagem C para manipulação de elementos double de um array.\
Considerando o escopo livre do trabalho, as seguintes funções foram criadas:

- **Offset:** adiciona um valor constante à variável de entrada.
```math
f(x) = x + 1.5
```

- **Scale:** amplifica a variável de entrada.
```math
g(x) = 3.3x
```
- **Linear:** aplica uma transformação linear na variável de entrada.
```math
h(x) = 3.3x + 1.5
```

### Parte 1

Na Parte 1 todas as funções foram criadas em um mesmo arquivo, juntamente com o `main`.\
Para compilar o código e executar o programa utilize os seguintes comandos no terminal:

```
cd parte1
gcc main.c -o main
./main
```

> **:warning: Atenção:**\
> Você deve executar esses comandos no terminal dentro da pasta do projeto.\
Caso você não tenha uma cópia local dos códigos utilize `git clone https://gitlab.com/ldf-sel0456/trabalho01` para baixar o repositório.

### Parte 2
Na Parte 2 as funções foram separadas em arquivos diferentes.\
Um `Makefile` está disponível para facilitar a compilação dos códigos.\
Utilize os seguintes comandos para executar o programa:
```
cd parte2
make all
./main
```
> **:information_source: Dica:**\
> Utilize o comando `make clean_obj` para apagar todos os arquivos `.o` criados durante a compilação do código principal.\
Para também excluir o executável utilize `make clean_all`.
